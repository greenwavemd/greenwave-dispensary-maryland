Greenwave Medical Cannabis Dispensary is conveniently located in Solomons Maryland. Greenwave's companywide mission is to offer consultation and access to approved patients of their state Medical Cannabis program. Greenwave intends to be an industry leader by providing all dispensary agents (patient care specialists) pre-employment and continuing training in FDA dispensing standards.

Greenwave's Dispensary Agent or Patient Care Specialists focus on quality assurance and patient care and strive to educate their patients on cannabinoid therapy and strains that target their particular illness and condition for direct alleviation. All medicinal products offered at Greenwave Dispensaries are first tested by an ISO Certified laboratory and clearly labeled with the test results. 

If you are a patient still on an endless search for relief, do yourself a favor and don't overlook this form of alternative therapy. The staff at Greenwave provides a discreet patient experience where the dispensary agents or patient care specialists will carefully explain the cannabis strains and their therapeutic benefits.

Make an appointment with one of our Solomons Maryland Dispensary Agents today and see if medical cannabis is right for you.

Address: 10 Creston Lane, Suite 4, Solomons, MD 20688, USA

Phone: 410-394-3936
